use bitflags::bitflags;

// Bit mask for transport specific flags in VirtIO feature set defined by vhost-user.
bitflags! {
    /// Transport specific flags in VirtIO feature set defined by vhost-user.
    pub struct VhostUserVirtioFeatures: u64 {
        /// Feature flag for the protocol feature.
        const PROTOCOL_FEATURES = 0x4000_0000;
    }
}
